module Gitlab
  module QA
    module Component
      class PostgreSQL < Base
        DOCKER_IMAGE = 'postgres'.freeze
        DOCKER_IMAGE_TAG = '11'.freeze

        def name
          @name ||= "postgres"
        end

        def start
          @docker.run(image, tag) do |command|
            command << "-d"
            command << "--name #{name}"
            command << "--net #{network}"

            command.env("POSTGRES_PASSWORD", "SQL_PASSWORD")
          end
        end

        def run_psql(command)
          @docker.exec(name, %(psql -U postgres #{command}))
        end

        private

        def wait_until_ready
          start = Time.now
          begin
            run_psql 'template1'
          rescue StandardError
            retry if Time.now - start < 30
            raise
          end
        end
      end
    end
  end
end
